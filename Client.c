//Client 

#include "LibreriaAmg.h"
#define RETURN_ERROR_INVIO\
		if( INVIATINumBytes < 0){\
			perror("");	printf("  ERRORI TRASMISSIONE");getchar();return;}\
			if( INVIATINumBytes == 0){\
				printf("OPERAZIONE  Invio FALLITA SERVER HA CHIUSO COLLEGAMENTO,ENTER");getchar();return;}\

#define RETURN_ERROR_RICEZ \
		if(RICEVUTINumBytes < 0){\
			printf("  ERRORI RICEZIONE");getchar();return;}\
			if(RICEVUTINumBytes == 0){ \
				printf("OPERAZIONE ricev FALLITA SERVER  HA CHIUSO COLLEGAMENTO,ENTER");getchar();return;}\

#define CLEARSCREENVT100 \
		printf("\n\033[2J");\
/*erase screen emullazione VT100 della shell*/\
		printf("\n \033[1;1H");\
/*prima riga 1 colonna emullazione VT100 della shell*/\

#define VISUALIZZA_POST_PRIMA_RIGA\
		printf("\n(%4d)   %-8s %-32s  %-76s",Post->NumOrdPost,Post->IdUtente,Post->mittente,Post->dataoggetto);

#define VISUALIZZA_TESTO\
		printf("\n%s\n",Post->testo);

int     INVIATINumBytes;//ritorni di  scritture su linea.se <1 anomalie
int     RICEVUTINumBytes;//ritorni di letture  su linea.se <1 anomalie
char 	TipoOper;
int 	SocketColloquio;//tra server e cliente
//********************************************
//********************************************

utente  Utente;   // Utente nuovo inin registrazione  o gia registrato e loggato
postlvar	*Post;//scambi con server
//essendo in ricezione si dimensiona per i malori massimi previsti  con testo e senza testo
//essendo in trasmissione nuovo post utilizza la dimensione massima 
int		DimPostMax=sizeof(postlvar)+LMAXTESTO;   //3124
int 	DimPostMin =sizeof(postlvar);//comprende anche il primo byte del campo testo variabile 124
int 	flagloggato=0; //il login avviene ad ogni operazione ma  l'utente non deve ripetere la digitazione delle credenziali
//======================PROTOTIPI  MODULI FUNZIONI
void AttivazioneCollegamento();
int  LoginCliente();
void Op_Vis_e_Canc_ConNumRiferim();
void OpListaSelettiva();
void OpAcquNuovoPost();
void OpRegistrazioneUt();

//FINE VARIABILI COMUNI ,DEFINE ,prototipi funzioniINIZIO MODULI------------------------------------------------------------
//=================================================================================================================================
/* CODici  EXIT
	0 chiusura normale
	1  err  socket
	2  err inet_aton
	3 err connect mpossibile connettersi al server
 */
//******************************************************************************************************************
//******             MAIN                                    ******************
//******************************************************************************************************************
int main(int argc, char **argv){
	char	indirizzo[9+1]= "127.0.0.1";//uso test
	Post= (postlvar *) malloc(DimPostMax);   
	if(Post == NULL) exit (1);

	//******************************************************************************************************************
	//******               CICLO INDEFINITO     DI SMISTANTO >>OFFLINE<<<< CON COD OPER   USCITA SOLO PER F             ******************
	//******************************************************************************************************************
	while(1)	 	{ // si esce solo per tipo ope F fine
		/*smistamento al tipo operazione ogni volta che si fa una nuova operazione
		 tra una  oper e l'altra il cliente si disconnette e si riconnette solo al momento di inviare i dati*/ 
		while(1){//ciclo indefinito esce per F
			CLEARSCREENVT100;
			printf("\n  BACHECA ELETTRONICA REMOTA CON LOGIN INIZIALE)  ");
			printf("\n1  ELENCO SELETTIVO POSTS");
			printf("\n2  SOLA VISUALIZZAZIONE POST");
			printf("\n3  INVIO NUOVO POST ");
			printf("\n4  REGISTRAZIONE NUOVO UTENTE ");
			printf("\n5  CANCELLAZIONE POST ");
			printf("\nU  LOGOUT DI USCITA DELL'UTENTE");
			printf("\nF  CHIUSURA");
			printf("\n_____________________________________________\n\n");
			char domanda1[]="\DIGITARE  UN TIPO OPERAZIONE VALIDO ";//...FlagScelta oper
			TipoOper=FinestraDialogo(domanda1,CARATTEREMAIUSC,1,NULL);
			if(TipoOper== 'F')	exit (0);
			if(TipoOper=='U') {
				printf ("LOGOUT, INVIO PER NUOVO LOGIN  O  PER CHIUDERE");
				getchar();
				flagloggato=0;
			}
			
			if(TipoOper>='1' && TipoOper<= '5' ) break;
			//se arriva qui ripete
		}
		CLEARSCREENVT100
		switch(TipoOper)
		{
		case '1':
			OpListaSelettiva();
			break;
		case '2':
		case '5':
			Op_Vis_e_Canc_ConNumRiferim();
			break;
		case '3':
			OpAcquNuovoPost();
			break;
		case '4':
			OpRegistrazioneUt();
			break;
		}
		close(SocketColloquio);//terminato servizio ad un cliente e se del caso avvisa il client di chiusura anticipata
		getchar();// per non far ricomparire subito la schermata iniziale senza poter leggere i risultati
	}
}

//******************************************************************************************************************
//******          ATTIVAZIONE COLLEGAMENTO COL SERVER CON INVIO     COD OPER     DOPO DIGITAZIONI ******************
//******************************************************************************************************************
void AttivazioneCollegamento(){
	int esito;
	char	indirizzo[9+1]= INDIRIZZOSERVER;
	struct sockaddr_in  name;
	SocketColloquio=socket(AF_INET,SOCK_STREAM,0);//TCP 									<<<<<<<<<creazione SOCKET
	if(SocketColloquio <0) {printf ("IERRORE PROGRAMMA: %d  \n", errno);exit (1);}
	name.sin_family=AF_INET;
	esito=inet_aton(indirizzo, &name.sin_addr) ;//											<<<<< impostazione indirizzo
	if(  esito == 0 ){printf ("ERRORE PROGRAMMA n : %d ,INVIO PER CHIUDERE",errno);exit (2);		}
	name.sin_port =htons (PORT);
	esito=connect (SocketColloquio, (struct sockaddr *) &name, sizeof (name));//			<<<<<RICHIESTA CONNESSIONE
	if ( esito < 0)			{
		perror ("IMPOSSIBILE CONNETTERSI AL SERVER  INVIO PER CHIUDERE;causa ");
		if(errno==110) {printf(" timeout durante il tentativo di connessione");getchar();exit (3);}
		if(errno==111) {printf(" il server indirizzato non  è ATTIVO  ");	getchar();exit (3);}
	}
	INVIATINumBytes=write(SocketColloquio, &TipoOper, sizeof(TipoOper));RETURN_ERROR_INVIO;
}
//******************************************************************************************************************
//****                     LOGIN                                                                   ******************
//******************************************************************************************************************
LoginCliente() {
	//CLEARSCREENVT100
	int  esito;
	/*1 OK .0  non OK perche finiti 3 tentativi*/
	char Stringalogin[LUSER+LPW+1],PWDIG[LPW+1];
	if (flagloggato==0)printf("\n===LOGIN INIZIALE UTENTE,TENTATIVI.AMMESSI %d  CON TIMEOUT \n ",NTENTLOGIN);
	for(int i=1;i<=NTENTLOGIN;i++){
		if (flagloggato==0){ //se era 1 usava le credenziali gia digitate
			FinestraDialogo ("IUSERID",STRINGA,LUSER,Utente.ID);
			FinestraDialogo ("PASSWORD",STRINGA,LPW,Utente.password );
		}
		INVIATINumBytes=write(SocketColloquio, &Utente, sizeof(Utente));RETURN_ERROR_INVIO;
		RICEVUTINumBytes=read(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_RICEZ;
		if(esito==1 )break;
	}
	if(esito==0)		printf("\nESAURITI TENTATIVI ,INVIO PER NUOVA OPERAZIONE",NTENTLOGIN);
	if(esito==1){		flagloggato=1;} //flag per il Riuso le credenziali gia salvate
}
//******************************************************************************************************************
//******                        OPER 1---ESTRAZIONI SELETTIVE                                                                  ******************
//******************************************************************************************************************
void	OpListaSelettiva() {
	//riceve solo la parte fissa del post cioè senza testo
	int esito,lun, UltimiPostDaEsaminare=0,p=0,ricevuti=0;
	char domanda1[]="  VISUALIZZA N ULTIMI  POSTS IN ARCHIVIO DIGITARE QUANTITA  o NL pER TUTTI.";
	UltimiPostDaEsaminare=FinestraDialogo (domanda1,NUMERIC,4, NULL);  //NUMERIC e' il flag che determina la verifica e conversione in numero 
	char 	domanda4[]="CERCARE IN USER";
	char StringaRicerU[LUSER+1];
	FinestraDialogo (domanda4,STRINGA,sizeof(  StringaRicerU  )-1,StringaRicerU);
	char 	domanda5[]="AND CERCARE IN MITTENTE";
	char StringaRicerM[LMITT+1];
	FinestraDialogo (domanda5,STRINGA,sizeof(  StringaRicerM  )-1,StringaRicerM);
	char 	domanda6[]="AND CERCARE IN DATA/OGGETTO ";
	char StringaRicerO[LDATAOGGETTO+1];
	FinestraDialogo (domanda6,STRINGA,sizeof(  StringaRicerO  )-1,StringaRicerO);
	char 	domanda7[]="AND CERCARE IN TESTO";
	char StringaRicerT[40+1];
	FinestraDialogo (domanda7,STRINGA,sizeof(  StringaRicerT  )-1,StringaRicerT);
	AttivazioneCollegamento();//con invio Tipo operazione corrisponde  all'interno del  ciclo main  server
	int esitologin= LoginCliente(); 
	if (esitologin == 0)	return;
	INVIATINumBytes=write(SocketColloquio, &UltimiPostDaEsaminare, sizeof(UltimiPostDaEsaminare));RETURN_ERROR_INVIO;
	INVIATINumBytes=write(SocketColloquio, StringaRicerU, sizeof(StringaRicerU));RETURN_ERROR_INVIO;////////
	INVIATINumBytes=write(SocketColloquio, StringaRicerM, sizeof(StringaRicerM));RETURN_ERROR_INVIO;////////
	INVIATINumBytes=write(SocketColloquio, StringaRicerO, sizeof(StringaRicerO));RETURN_ERROR_INVIO;////////
	CLEARSCREENVT100;
	INVIATINumBytes=write(SocketColloquio, StringaRicerT, sizeof(StringaRicerT));RETURN_ERROR_INVIO;////////
	putchar('\n');for(int i=0;i<=160;i++) putchar(	'_');//riga separazione
	printf("\n POST SELEZIONATI  ");
	printf("\nRIFER     USERID    MITTENTE                           DATA e OGGETTO");//intestazioni
	putchar('\n');for(int i=0;i<=160;i++) putchar(	'_');//riga separazione
	p=1;
	while(1)	{			//senz fine salvo uscita break per fine lista
		RICEVUTINumBytes=read(SocketColloquio, Post, DimPostMin);RETURN_ERROR_RICEZ;
		//riceve solo la parte fissa senza testopari alla dimensione minima
		if(Post->NumOrdPost==-1)break;//significa finelista
		ricevuti++;
		VISUALIZZA_POST_PRIMA_RIGA
		p++ ;
	}
	putchar('\n');for(int i=0;i<=160;i++) putchar('_');//riga chiusura
	printf("\nTROVATI  %d POSTS NON CANCELLATI SU  %d esaminati " ,p-1,UltimiPostDaEsaminare);
}
//******************************************************************************************************************
//******                          OPER 2 SOLO LETTURA --5 CON CODA CANCELLAZIONE CON PW RIDIGITATA                                                            ******************
//******************************************************************************************************************
void Op_Vis_e_Canc_ConNumRiferim() {
	//la funzione solo consultazione ha un cod oper suo ed esplicito per evitare cancellazioni involontarie
	int lun;
	char 	RigadaRiempire[100+1];RigadaRiempire[100]='\0';//renderlo stringa  
	printf("\n");
	char domanda1[]="NUMERO ->ATTUALE<- DI LISTA PER INDIVIDUAZIONE DEL POST  ";
	int esito;
	int NumReg=	FinestraDialogo (domanda1,NUMERIC,4, NULL);  // domanda e fa la cnversione 
	AttivazioneCollegamento();
	int esitologin= LoginCliente(); ;
	if (esitologin == 0 ){	return;}
	INVIATINumBytes=write(SocketColloquio, &NumReg, sizeof(NumReg));RETURN_ERROR_INVIO;
	RICEVUTINumBytes=read(SocketColloquio, Post, DimPostMax);RETURN_ERROR_RICEZ;//riceve l'intero post quindi predisposto alla lunghmax messaggio
	//..........ricezione delPOST, con num progr -1 se inesist
	if(Post->NumOrdPost==-1){printf("\n\n\n NON PIU CONSULTABILE  O RIFERIMENTO ERRATO ");return;	}
	CLEARSCREENVT100;
	printf("\nRIFER     USERID    MITTENTE                            DATA e OGGETTO");//intestazioni
	VISUALIZZA_POST_PRIMA_RIGA
	putchar('\n');for(int i=0;i<=160;i++) putchar('_');
	VISUALIZZA_TESTO
	if(TipoOper=='2')	{	
		printf ("\n\n INVIO PER NUOVA OPERAZIONE");
		return;
		}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>segue coda per cancellazione tipo5
	esito=strcmp(Post->IdUtente,Utente.ID);// verifica abilitazione a cancellare
	if (esito!=0) {printf("\n NON ABILITATO A CANCELLARE");return;}
	//dopo return NEL CICLO c'è close(SocketColloquio) che  avverte il server (ONERROREREAD num car=0)in attesa read che passa a trattare nuova richiesta
	char pwcanc[LPW+1];
	printf("\n _____________________");
	char domanda2[]="\nCONFERMI LA RIMOZIONE DI QUANTO VISUALIZZATO(CON TIME OUT)?  S/N";
	char ConfermaCancellazione=  FinestraDialogo(domanda2,CARATTEREMAIUSC,1,NULL);// C trattae i caratteri anche come num su un byte
	if (ConfermaCancellazione !='S' ) printf("\n CANCELLAZIONE NON PIU RICHIESTA");
	INVIATINumBytes=write(SocketColloquio, &ConfermaCancellazione, sizeof(ConfermaCancellazione));RETURN_ERROR_INVIO;
	RICEVUTINumBytes=read(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_RICEZ;
	printf("\n");
	if(esito==1 ) printf("\nCANCELLATA CONFERMATA ");
	return;
}
//******************************************************************************************************************
//******                         OPER 3 NUOVO POST                                                                  ******************
//******************************************************************************************************************
void OpAcquNuovoPost(){
	CLEARSCREENVT100;
	printf("\nCOMPILAZIONE TESTO  PRIMA DI CONNETTERSI\n");
	FinestraDialogo ("MITTENTE ",STRINGA,LMITT,Post->mittente);
	FinestraDialogo ("OGGETTO  ",STRINGA,LDATAOGGETTO,Post->dataoggetto);
	CLEARSCREENVT100;
	char domandat[]="TESTO  !!PER TERMINARE ^ INVIO \n";
	FinestraDialogo (domandat, STRINGA_MULTIRIGA,LMAXTESTO,Post->testo);
	Post->NumOrdPost=0;
	AttivazioneCollegamento();  
	int esitologin= LoginCliente(); 
	if (esitologin == 0 ){	return;}
	strcpy(Post->IdUtente,Utente.ID);
	INVIATINumBytes=write(SocketColloquio, Post, DimPostMax);RETURN_ERROR_INVIO;
	//int esito;
	//RICEVUTINumBytes=read(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_RICEZ;
	printf("\nACQUISIZIONE EFFETTUATA");
	return ;
}
//******************************************************************************************************************
//******                               OPER 4 REGISTRAZIONE NUOVO UTENTE                                                           ******************
//******************************************************************************************************************
void OpRegistrazioneUt(){
	int esito;
	CLEARSCREENVT100
	printf("\n NUOVA REGISTRAZIONE RICHIESTA");
	FinestraDialogo ("NUOVO IUSERID",STRINGA,LUSER,Utente.ID);
	FinestraDialogo ("PASSWORD ",STRINGA,LPW,Utente.password );
	//FinestraDialogo ("INFORMAZIONI ANAGRAFICHE",STRINGA,LANAGR,Utente.DatiAnagrafici);
	AttivazioneCollegamento();
	//NESSUN LOGIN che il server per la op4 infatti non attende
	INVIATINumBytes=write(SocketColloquio, &Utente, sizeof(Utente));RETURN_ERROR_INVIO;
	RICEVUTINumBytes=read(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_RICEZ;
	if(esito != 0){
		flagloggato=1;
		printf("\nNUOVA REGISTRAZIONE EFFETTUATA ,EFFETTUATO IL LOGIN",
				esito,flagloggato);
	}
	else 	 printf("\n UTENTE GIA REGISTRATO");
	return ;
}
