//LibreriaAmg.h 
//#include  <unistd.h>
#include  <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>   
#include <time.h>
#include <string.h>
#include <errno.h>   
#include <signal.h> 
#include <arpa/inet.h> 
#include <sys/socket.h> 

//parametri connessione
#define PORT 25000
#define INDIRIZZOSERVER   "127.0.0.1"
// dimensioni sel set posts
#define CAPIENZAVETTORE 3000
#define MARGINE    500
#define INTERVALLOSALVATAGGI  50  // num post aggiunte o cancellazioni tra due salvataggi autom in bacheca

//lungh dati
#define LPROGR 4 
#define LUSER 8
#define LMITT 32
#define LDATAOGGETTO 76
#define LMAXTESTO 3000 
#define LPW 8
//flags routine acquisizione
#define STRINGA 1
#define NUMERIC 2
#define STRINGA_MULTIRIGA 3
#define CARATTEREMAIUSC 4
//time out operazioni su server
#define TIMEOUTCANC 60//per confermare cancellazione 5
#define TIMEOUTlogin 60// inoltre NTENTLOGIN tentativi
#define NTENTLOGIN 3 //nim max tentativi per sincrizzare server e cliente,oltra a ltine out

typedef struct{//sizeof(postlvar=124)+stringa variabile testo senza terminatore(max3000)
	int  NumOrdPost;//serve al cliente come indice attuale 
	char IdUtente[LUSER+1];    
	char mittente[LMITT+1];
	char dataoggetto[LDATAOGGETTO+1];
	char testo[];  	//alloca comunque il primo elemento 
}postlvar;
//la dimensione variabile è sfruttata per il set in memoriae la trasmisssione al cliente,nel cliente nei buffer di linea si utilizza la max dimensione o quella senza testo
typedef struct{//79
	char	 ID [LUSER+1];
	char      password[LPW+1];
}utente;

int FinestraDialogo (char *,int ,int,char *);
