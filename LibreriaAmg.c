//LibreriaAmg.c

#include "LibreriaAmg.h"
int FinestraDialogo (char domanda[],int tipocampo,int LungMaxDigita, char Campodigiest[]){
	//Campodigiest[] nullo se numeric o carattere
	int i, numero,lunghguida; 
	char  ZeroTermStringa='\0', NewLine ='\n', Spazio=' ';
	char CampodigitInterno[6];
	//composizione eventuale guida e richiesta di campo
	if (tipocampo==STRINGA_MULTIRIGA)	lunghguida = 0; else lunghguida=LungMaxDigita;
	putchar('\n');for(int i=0;i<lunghguida;i++) putchar('_');printf("%s",domanda );	//tetto guida ev nulla  e richiesta
	putchar('\n');for(int i=0;i<lunghguida;i++) putchar('.');printf("|\r");//ev puntini guida e ritorno in prima

	//TRATTAMENO DEI VARI TIPI CAMPO
	//////////////////////////////
	if( tipocampo==CARATTEREMAIUSC){
		char carat=toupper(getchar());
		if (carat==ZeroTermStringa) carat = Spazio ;//caso digitazione nulla ritorna spazio
		if ( carat != '\n') while ( getchar() != NewLine ); // smaltire carat   sino a NL compreso
		return (int) carat;//casting
	}

	if(tipocampo==STRINGA )	{
		char como;
		for(i=0; i<LungMaxDigita;i++){
			como=getchar();
			if (como=='\n')break;else Campodigiest[i]=como;//ev uscita anticip con break dal for per fine digitazione con invio
		}
		Campodigiest[i]=ZeroTermStringa;
		if ( i==LungMaxDigita) while ( getchar() != NewLine ); //uscita per riemp completo od eccedente campo, smaltire carat   sino a NL
		return i;//ritorna lunghezza acquisita
	}

	if( tipocampo==NUMERIC){
		char como;
		for(i=0; i<LungMaxDigita;i++){
			como=getchar();
			if (como=='\n') break; else	CampodigitInterno[i]=como;
		}
		CampodigitInterno[i]=ZeroTermStringa;
		if ( i==LungMaxDigita) while ( getchar() != NewLine ); //uscita per riemp completo od eccedente campo, smaltire carat   sino a NL
		return atoi(CampodigitInterno); // se campo erraro torna zero
	}


	if(tipocampo==STRINGA_MULTIRIGA) {
		char como;
		for(i=0; i<LungMaxDigita;i++){	//fino a CARATTEREMAIUSC fine da sostiruire con zeroterninazione
			como=getchar();
			if (como=='^') break; else Campodigiest[i]=como;
		}
		Campodigiest[i]=ZeroTermStringa;
		while ( getchar() != NewLine ); //uscita per riemp completo od eccedente campo, smaltire carat   sino a NL
		return i;//ritorna lunghezza netta acquisita
	}


}//finemodulo
