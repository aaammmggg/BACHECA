//SERVER  
#include "LibreriaAmg.h"
//**********************************************
#define RETURN_ERROR_INVIO \
		if(INVIATINumBytes < 0){\
			printf("ALTRI ERRORI    INVIO");return;}\
			if(INVIATINumBytes == 0){\
				printf("\nINVIO FALLITO CLIENT HA CHIUSO COLLEGAMENTO");	return ; }

#define RETURN_ERROR_RICEZ \
		if(RICEVUTINumBytes < 0){\
			printf(" ALTRI ERRORI RICEZIONE");return;}\
			if(RICEVUTINumBytes == 0){\
				printf("RICEZIONE FALLITA CLIENT  HA CHIUSO COLLEGAMENTO  ");return;}

FILE	 *fpost,*fUtentiReg;
int      IndiciPostsUsati=0;//Totale Posts numerati anche se cancellati
int      margine=500;//ricostrituito dopo saturazione ochiusuracon elim antichi
int      FLAGcaricatiposts=0;
int      INVIATINumBytes;//ritorni di letture e scritture su linea
int      RICEVUTINumBytes;//ritorni di letture e scritture su linea
int 	 SocketColloquio; //tra server e cliente connesso
int 	 SocketAscolto;
char 	 TipoOper;
int 	ContaCancAggiun=0;
int		DimPostMin=  sizeof(postlvar); //124
int		DimPostMax=  sizeof(postlvar)+LMAXTESTO;//3124
postlvar   *Post, *PostMem[CAPIENZAVETTORE]; //struct di tipo Flexible array member e quindi di lungh variabile
utente   Utente,UtenteRichiedente;
//=======================PROTOTIPI  MODULI FUNZIONI

void Caric_bacheca_permanente_in_mem();
int  LoginCliente();
void OpListaSelettiva();
void Op_Vis_e_Canc_ConNumRiferim();
void OpAcquNuovoPost();
void OpRegistrazioneUt();
void gestore_SIGALRM();
void gestore_chiusura ();
void CompattaIndici();
void CreaNuovaBacheca();

/*
CODICI DELLE EXIT
	0 chiusura con control C
	1 errore malloc
	2 errore archivio Bacheca
	3 errore archivio utenti
	5 errore creazione socket di ascolto su porta
	6 errore bind porta in uso
	7 errore listen
	8 errore creazione socket con cliente per accept
 */

//******************************************************************************************************************
//******    MAIN INIZIALIZZATORE E CICLO ATTESA E SERVIZIO     MAIN                                                                             ******************
//******************************************************************************************************************
int main(int argc, char **argv) {
	printf("\n%d-%d",sizeof( postlvar),DimPostMax);
	Post= (postlvar *) malloc(DimPostMax);   // usato con 'archivio permanente o per segnalae alcliente post inesistente
	struct sigaction salrm;
	salrm.sa_handler = &gestore_SIGALRM; 
	salrm.sa_flags = SA_RESTART; 
	sigfillset(&salrm.sa_mask);   
	sigaction(SIGALRM,&salrm,NULL);
	struct sigaction sacc;
	sacc.sa_handler = &gestore_chiusura;
	sacc.sa_flags = SA_RESTART; 
	sigaction(SIGHUP,&sacc,NULL);	
	signal(SIGINT,gestore_chiusura);
	//******************************************************************************************************************
	//******      APERTURA PORTA DI ASCOLTO                                                                                    ******************
	//******************************************************************************************************************
	struct sockaddr_in IndCliente, IndServer;//gestiscono indirizzi
	SocketAscolto = socket (AF_INET,SOCK_STREAM,0);  //istruzione per il TCP
	if (SocketAscolto ==-1)	{
		printf ("\nSOCKET NON PUO ESSERE CREATO%d\n",errno);exit (5);
	}
	IndServer.sin_family = AF_INET;
	IndServer.sin_addr.s_addr=htonl (INADDR_ANY);  //host to network long32b
	IndServer.sin_port= htons (PORT);	/*htons host to network short 16-bit  */
	int esito;
	esito=bind (SocketAscolto, (struct sockaddr *) &IndServer, sizeof (IndServer));//<<<<<<<<<Assegnazione porta
	if (esito<0){
		printf ("ERRORE BIND %d EADDRINUSE\n ",errno);
		getchar();//PAUSA;	
		exit (6);
	}
	esito=listen (SocketAscolto,5);// attesa richieste di connessione ,5 max accodamenti nel kernel a cura listen
	if (esito<0) {
		perror("\n ERRORE listeN\n");
		printf ("%d eerno",errno);	exit (7);
	}
	//******  i due archivi permanenti                   ******************
	fUtentiReg=fopen ("UtentiRegistrati","ab+");if (fUtentiReg==NULL) 	printf("INSTALLAZIONE UTENTI  CREAT0 VUOT0\n");
	Caric_bacheca_permanente_in_mem()	;//CARICAMENTO  SET POSTS  IN MEMORIA  CON VETTORE PUNTATORI  
	//******************************************************************************************************************
	//**  CICLO  ATTESA  ACCEPT E SERVIZIO  TIPO OPERAZIONE CLIENTI  CON CONTROLLI SUL RIIEMPIMENTO BACHECA                   ******************
	//******************************************************************************************************************

	while(1) { 
		//****************copia periodica in bacheca ogni 50 aggiunte e cancellazioni
		if( ContaCancAggiun==INTERVALLOSALVATAGGI){
			CreaNuovaBacheca();	//salvataggio cadenzato
			ContaCancAggiun=0;	
		}
		//**********************azione puliziA  se saturato l'indice del vettor con modifica dei riferimenti
		if(IndiciPostsUsati>=CAPIENZAVETTORE ){	
			printf("\n-------------------saturo il puntatore,\n");
			CompattaIndici();//crea  posizioni libere eliminandi parte dei vecchi
			printf("\nscrittur bacheca\n");
		}
		//**************************************************************
		int lun = sizeof(struct sockaddr_in);
		SocketColloquio = accept(SocketAscolto,(struct sockaddr *) &IndCliente,&lun);//********ACCEPT IN ATTESA
		if (SocketColloquio < 0){			printf ("%d errno",errno);exit (8);}
		RICEVUTINumBytes=read(SocketColloquio, &TipoOper, sizeof(TipoOper));RETURN_ERROR_RICEZ;
		printf ("\n Tipo Operazione %c \n",TipoOper);
		int esitologin;
		if(TipoOper!='4') 	{
			esitologin= LoginCliente();
			if (esitologin==0)	continue;//non validato
		}
		switch (TipoOper)	{
		case '1':		
			OpListaSelettiva();
			break;
		case '2':	//lettura
		case '5':	//lettura seguita da cancellazione
			Op_Vis_e_Canc_ConNumRiferim();
			break;
		case '3':
			OpAcquNuovoPost();
			break;
		case '4':	
			OpRegistrazioneUt()   ;
			break;
		}
		close(SocketColloquio);//se uscito per anomalia lasciando in attesa il cliente comunque forza qui la chiusura
	}//torna ad attendere un tipo oper da un nuovo  cliente------ciclo senza fine 
}

//******************************************************************************************************************
//****** DA BACHECA  CARICAMENTO NEL SET IN MEMEMORIA CON  CREAZIONE VETTORE PUNTATORI ******************
//******************************************************************************************************************
void Caric_bacheca_permanente_in_mem(){ 
	//
	system("cp Bacheca Bachecabackup");
	fpost=fopen ("Bacheca","rb");	
	if (fpost==NULL) {
		fpost=fopen ("Bacheca","ab");	fclose(fpost);
		printf("Prima installazione nessun  post presente\n");
		return;// non c'è nulla da caricare

	}
	//ciclo caricamento se esistetnet bacheca permanenet
	int NumOrdine=0;
	while(fread(Post,DimPostMax,1,fpost) ){		
		if(ferror(fpost)) exit (2);
		// lo carico  nel set bacheca in memoria 
		int DimPost=DimPostMin+strlen(Post->testo);
		PostMem[NumOrdine]= (postlvar *) malloc(DimPost);
		if(PostMem[NumOrdine] == NULL) exit (1);
		//copia tra due struct variabili
		(*PostMem[NumOrdine])= (*Post);// = copia solo parte dichiarata fissa  (124 bytes),senzatesto,solo 1
		strcpy(PostMem[NumOrdine]->testo,Post->testo);// testo variabile non contemplata dalla ugualianza suscritta che lo assume a zerocar
		//
		printf("\ndim %d \t(%4d)1t   %-8s %-32s  %-76s",DimPost,NumOrdine,PostMem[NumOrdine]->IdUtente,PostMem[NumOrdine]->mittente,PostMem[NumOrdine]->dataoggetto);
		NumOrdine++;
	}
	fclose(fpost);
	IndiciPostsUsati=NumOrdine;
	printf("\n=== %d POSTSNELLA BACHECA su capienza indice %d\n\n",	IndiciPostsUsati,CAPIENZAVETTORE);
	FLAGcaricatiposts=1;
}
//°°°°°°°°°°°°°°°°°°°°°°°°GESTORI SEGNALI
void gestore_SIGALRM(){
	printf("\n\n TIME OUT");
	close(SocketColloquio); 
}

//-----------------------------
void gestore_chiusura(){
	CompattaIndici() ;//ricostituendo margine
	CreaNuovaBacheca();
	printf("\n*exit per uscire");getchar();
	exit(88);
}

//******************************************************************************************************************
//******                        LOGIN                                                             ******************
//******************************************************************************************************************
int LoginCliente(){
	int esito ;
	for(int i=1;i<=NTENTLOGIN;i++){	
		esito=0;
		alarm(TIMEOUTlogin);
		int DimArchUtenti=sizeof(utente);
		rewind(fUtentiReg);
		RICEVUTINumBytes=read(SocketColloquio, &UtenteRichiedente, sizeof(UtenteRichiedente));RETURN_ERROR_RICEZ;
		while(1)	{ //loop senza fine salvo break= trovato o fine =non registrato
			fread(&Utente,DimArchUtenti,1,fUtentiReg);if(ferror(fUtentiReg)){ exit (3);}//
			if(feof(fUtentiReg)){ printf("\n autenticazione fallita"); break;}
			if (strcmp(Utente.ID,UtenteRichiedente.ID)==0 && strcmp(Utente.password,UtenteRichiedente.password)==0){
				esito=1;
				break;//esce dal ciclo corrente con trovato
			}
		}//fine while cioè di un tentativo validazione fallito con esito 0
		INVIATINumBytes=write(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_INVIO;
		alarm(0);
		if(esito == 1)	break;//esce anche da while e modulo  per trovato
	}//FUORI DAL LOOP TENTATIVI
	return esito;// 1 registrato  0 non registrato
}

//******************************************************************************************************************
//******                       oper 1                                                                   ******************
//******************************************************************************************************************
void OpListaSelettiva (){//sul set di posts im memoria
	int  esito,UltimiPostDaEsaminare;
	char StringaRicerU[LUSER+1];
	char StringaRicerM[LMITT+1];
	char StringaRicerO[LDATAOGGETTO+1];
	char StringaRicerT[40+1];
	RICEVUTINumBytes=read(SocketColloquio, &UltimiPostDaEsaminare, sizeof(UltimiPostDaEsaminare));RETURN_ERROR_RICEZ;
	RICEVUTINumBytes=read(SocketColloquio, StringaRicerU, sizeof(StringaRicerU));RETURN_ERROR_RICEZ;
	RICEVUTINumBytes=read(SocketColloquio, StringaRicerM, sizeof(StringaRicerM));RETURN_ERROR_RICEZ;
	RICEVUTINumBytes=read(SocketColloquio, StringaRicerO, sizeof(StringaRicerO));RETURN_ERROR_RICEZ;
	RICEVUTINumBytes=read(SocketColloquio, StringaRicerT, sizeof(StringaRicerT));RETURN_ERROR_RICEZ;
	int contatore=0,inizio;
	if (UltimiPostDaEsaminare==0) inizio=0;
	 else
	{ inizio=IndiciPostsUsati-UltimiPostDaEsaminare;
	if (inizio<0) inizio=0;}
	for(int i=inizio;i<IndiciPostsUsati;i++)	{
		if(PostMem[i]==NULL ) continue;//se cancellato non esiste più PostMem allocato e puntato
		contatore++ ;
		//esito=0;   
		if(		strstr(PostMem[i]->mittente,StringaRicerM) !=NULL     //strstr verifica se la stringa e' contenuta, anche in caso di stringa nulla 
				&&	strstr(PostMem[i]->dataoggetto,StringaRicerO) !=NULL
				&&	strstr(PostMem[i]->IdUtente,StringaRicerU) !=NULL
				&&	strstr(PostMem[i]->testo,StringaRicerT) !=NULL)	{
			//esito=1;
			PostMem[i]->NumOrdPost=i;                         //contiene l'indice attuale da passare al cliente per selezionare
			INVIATINumBytes=write(SocketColloquio, PostMem[i], DimPostMin);RETURN_ERROR_INVIO; //mando il post senza testo
			//printf("\ndim%d\n",DimPostMin);

		}
	}
	Post->NumOrdPost=-1; //ritornato Un Post dim max di comodo con NumOrdPost-1 se  finelista
	INVIATINumBytes=write(SocketColloquio,Post, DimPostMin);RETURN_ERROR_INVIO;

}	
//******************************************************************************************************************
//******                        op 2                                                                  ******************
//******************************************************************************************************************
void Op_Vis_e_Canc_ConNumRiferim(){
	int i;//i è il numero di registrazione che identifica il post,parte da 0
	RICEVUTINumBytes=read(SocketColloquio, &i, sizeof(i));RETURN_ERROR_RICEZ;
	if(i>IndiciPostsUsati   ||PostMem[i]==NULL) {	
		Post->NumOrdPost=-1; //ritornato Un Post dim min di comodo con NumOrdPost -1 se  inesistente
		INVIATINumBytes=write(SocketColloquio,Post, DimPostMin);RETURN_ERROR_INVIO;
		//printf("\ndimerrore%d\n",DimPostMin);
		return ;
	}
	int	DimPost=  DimPostMin+strlen(PostMem[i]->testo);  
	//printf("\ndim%d\n",DimPost);

	PostMem[i]->NumOrdPost=i;
	INVIATINumBytes=write(SocketColloquio, PostMem[i],DimPost );RETURN_ERROR_INVIO;
	if(TipoOper!='5') return;//evita cancellazioni involontarie

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>segue coda per cancellazione tipo5
	alarm(TIMEOUTCANC);
	char  ConfermaCancellazione; 
	RICEVUTINumBytes=read(SocketColloquio, &ConfermaCancellazione, sizeof(ConfermaCancellazione));RETURN_ERROR_RICEZ;
	int esito;
	alarm(0);
	if(ConfermaCancellazione!='S') esito=0;
	else{
		esito=1;
		free(PostMem[i]);// libera lo spazio allocato per il postlvar ma non lo pulisce  resta indirizzabile
		PostMem[i]=NULL;//cosi diventa inaccessibile cancellando il suo indirizzo puntatore
		ContaCancAggiun++;
	}

	INVIATINumBytes=write(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_INVIO;
	return;
}
//******************************************************************************************************************
//******                     OPERAZIONE 3                                                                     ******************
//******************************************************************************************************************
void OpAcquNuovoPost (){
	//int esito;
	//******************SCRITTURA SU FILE  e in MEMORIA CON RELATIVI PUNTATORI********
	RICEVUTINumBytes=read(SocketColloquio, Post, DimPostMax);RETURN_ERROR_RICEZ;
	//allocazione spazio
	int DimPost=DimPostMin+strlen(Post->testo);
	//printf("\ndim%d\n",DimPost);
	PostMem[IndiciPostsUsati]= (postlvar *) malloc(DimPost);
	if(PostMem[IndiciPostsUsati] == NULL) exit (1);
	Post->NumOrdPost =0;           //in scrittura setto a zero , serve solo come indice  se mandado al cliente 
	//copia tra due struct variabili
	(*PostMem[IndiciPostsUsati])= (*Post);// = copia solo parte dichiarata fissa  (124 bytes),1 per il testo
	strcpy(PostMem[IndiciPostsUsati]->testo,Post->testo);// copia testo variabile non prevista dall'uuaglinza precedente
	//
	IndiciPostsUsati++ ;
	//esito=0;
	//INVIATINumBytes=write(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_INVIO;
}
//******************************************************************************************************************
//******                       oper 4                                        limitedisicurezza                     ******************
//******************************************************************************************************************
void OpRegistrazioneUt(){
	RICEVUTINumBytes=read(SocketColloquio, &UtenteRichiedente, sizeof(UtenteRichiedente));RETURN_ERROR_RICEZ;
	int esito,esitoc;//
	fseek(fUtentiReg,0,SEEK_SET); //posiziona all'inizio
	int DimArchUtenti=sizeof(utente);
	while(1)	{ //loop senza fine salvo break trovato o fine fil=NON TROV
		fread(&Utente,DimArchUtenti,1,fUtentiReg);
		if(feof(fUtentiReg))  {esito=1;break;}//ok non presente in archivio esito 1
		esitoc=strcmp(Utente.ID,UtenteRichiedente.ID);
		if (esitoc==0 ){esito=0;	break;}//esistente quindi errore esito 0
	}
	if (esito ==  1){//nuovo
		fseek(fUtentiReg,0,SEEK_END);//in coda
		fwrite(&UtenteRichiedente,DimArchUtenti,1,fUtentiReg);if(ferror(fUtentiReg)){ exit (3);}//
		fflush	(fUtentiReg);//per sollecitare scaricamento buffer
	}
	printf("\nESITO=%d",esito);
	INVIATINumBytes=write(SocketColloquio, &esito, sizeof(esito));RETURN_ERROR_INVIO;
	ContaCancAggiun++;
	return ;
}

//////////////////////////////////////////
void CompattaIndici(){//
	int inuovo=0;//inizio svecchiamento  posts se esaurita capienza array
	int eliminandi=IndiciPostsUsati-(CAPIENZAVETTORE-MARGINE);//solo in chiusura
	if(eliminandi<=0 )eliminandi=0;
	printf("\n COMPATTAMENTO INDICE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	if (eliminandi <=0) printf("\n--------------------------------- nessun ELIMINAZIONE VECCHI");
	for( int i=0;i<eliminandi;i++) {//elimina i piu antichi
		printf("\n elim %d \t(%4d)1t   %-8s %-32s  %-76s ",i,i,PostMem[i]->IdUtente,PostMem[i]->mittente,PostMem[i]->dataoggetto);
		free(PostMem[i]);
		PostMem[i]=NULL;
	}
	for( int i=eliminandi;i<IndiciPostsUsati;i++) {
		if(PostMem[i] == NULL){printf("\nex %d cancellato",i); continue;}
		PostMem[inuovo]=PostMem[i]; //sposta indice a post  rinumeratozione
		printf("\n%d-> \t(%4d) %-8s %-32s  %-76s",i,inuovo,PostMem[inuovo]->IdUtente,PostMem[inuovo]->mittente,PostMem[inuovo]->dataoggetto);
		inuovo++;
	}
	IndiciPostsUsati=inuovo;

}
//-----------------------------
void CreaNuovaBacheca(){
	//solo scaricamento
	int scaricati=0;
	if(FLAGcaricatiposts==0) return;// scarica  solo se prima caricato set altrimenti la riscriverebbe errnesamente vuota
	fpost=fopen("Bacheca","wb");
	for( int i=0;i<IndiciPostsUsati;i++) {
		if(PostMem[i] == NULL){printf("\n NULL ex cancellato"); continue;}
		fwrite(PostMem[i],DimPostMax,1,fpost);if(ferror(fpost)) exit (2);
		printf("\n%(%4d) %-8s %-32s  %-76s",i,PostMem[i]->IdUtente,PostMem[i]->mittente,PostMem[i]->dataoggetto);
scaricati++;
	}
	printf("\n >>>>scaricanento %d post in bacheca",scaricati);
	fclose(fpost);
}
